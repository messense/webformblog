﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormBlog
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ObjectDataSource ods = new ObjectDataSource("BlogDataSetTableAdapters.postTableAdapter", "GetPagedData");
            ObjectDataSource ods = new ObjectDataSource(typeof(PagedPostList).AssemblyQualifiedName, "GetPagedData");
            ods.SelectCountMethod = "QueryCount";
            ods.EnablePaging = true;
            PostListView.DataSource = ods;
            PostListView.DataBind();
        }
    }

    class PagedPostList
    {
        private BlogModel.BlogEntities db = null;

        public PagedPostList()
        {
            db = new BlogModel.BlogEntities();
        }

        public int QueryCount()
        {
            return db.posts.Count();
        }

        public IQueryable GetPagedData(int startRowIndex, int maximumRows)
        {
            var posts = (from post in db.posts
                         orderby post.id descending
                         select new
                         {
                             id = post.id,
                             title = post.title,
                             created_at = post.created_at,
                             excerpt = post.excerpt,
                             content = post.content,
                             commentcount = post.commentcount,
                             catid = post.catid,
                             cattitle = post.category.title
                         }).Skip(startRowIndex).Take(maximumRows);
            return posts.AsQueryable();
        }
    }
}