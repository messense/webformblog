USE [blog]
GO
/****** Object:  Table [dbo].[member]    Script Date: 07/27/2013 10:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[member]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[member](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](20) NOT NULL,
	[password] [char](32) NOT NULL,
	[nickname] [varchar](20) NULL,
	[created_at] [smalldatetime] NOT NULL,
	[description] [varchar](200) NULL,
	[email] [varchar](50) NOT NULL,
 CONSTRAINT [PK__member__3213E83F7F60ED59] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__member__F3DBC572023D5A04] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[link]    Script Date: 07/27/2013 10:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[link]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[link](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL,
	[url] [varchar](200) NOT NULL,
	[imgurl] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[category]    Script Date: 07/27/2013 10:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NULL,
	[description] [varchar](200) NULL,
	[slug] [varchar](50) NULL,
	[postcount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[post]    Script Date: 07/27/2013 10:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[post]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[post](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[catid] [int] NOT NULL,
	[title] [varchar](100) NULL,
	[slug] [varchar](100) NULL,
	[content] [text] NOT NULL,
	[excerpt] [varchar](500) NULL,
	[created_at] [smalldatetime] NOT NULL,
	[updated_at] [smalldatetime] NULL,
	[format] [varchar](20) NULL,
	[thumbnail] [varchar](200) NULL,
	[commentcount] [int] NOT NULL,
	[uid] [int] NOT NULL,
 CONSTRAINT [PK__post__3213E83F0DAF0CB0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[comment]    Script Date: 07/27/2013 10:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[comment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[postid] [int] NOT NULL,
	[uid] [int] NULL,
	[nickname] [varchar](20) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[url] [varchar](100) NULL,
	[content] [text] NOT NULL,
	[created_at] [smalldatetime] NULL,
 CONSTRAINT [PK__comment__3213E83F117F9D94] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__category__postco__15502E78]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__category__postco__15502E78]') AND parent_object_id = OBJECT_ID(N'[dbo].[category]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__category__postco__15502E78]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[category] ADD  DEFAULT ((0)) FOR [postcount]
END


End
GO
/****** Object:  Default [DF__comment__uid__1920BF5C]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__comment__uid__1920BF5C]') AND parent_object_id = OBJECT_ID(N'[dbo].[comment]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__comment__uid__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[comment] ADD  CONSTRAINT [DF__comment__uid__1920BF5C]  DEFAULT ((0)) FOR [uid]
END


End
GO
/****** Object:  Default [DF__comment__created__1A14E395]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__comment__created__1A14E395]') AND parent_object_id = OBJECT_ID(N'[dbo].[comment]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__comment__created__1A14E395]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[comment] ADD  CONSTRAINT [DF__comment__created__1A14E395]  DEFAULT (getdate()) FOR [created_at]
END


End
GO
/****** Object:  Default [DF_member_created_at]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_member_created_at]') AND parent_object_id = OBJECT_ID(N'[dbo].[member]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_member_created_at]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_created_at]  DEFAULT (getdate()) FOR [created_at]
END


End
GO
/****** Object:  Default [DF__post__created_at__173876EA]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__post__created_at__173876EA]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__post__created_at__173876EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[post] ADD  CONSTRAINT [DF__post__created_at__173876EA]  DEFAULT (getdate()) FOR [created_at]
END


End
GO
/****** Object:  Default [DF__post__commentcou__182C9B23]    Script Date: 07/27/2013 10:19:32 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__post__commentcou__182C9B23]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__post__commentcou__182C9B23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[post] ADD  CONSTRAINT [DF__post__commentcou__182C9B23]  DEFAULT ((0)) FOR [commentcount]
END


End
GO
/****** Object:  ForeignKey [fk_comment_postid]    Script Date: 07/27/2013 10:19:32 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_comment_postid]') AND parent_object_id = OBJECT_ID(N'[dbo].[comment]'))
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [fk_comment_postid] FOREIGN KEY([postid])
REFERENCES [dbo].[post] ([id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_comment_postid]') AND parent_object_id = OBJECT_ID(N'[dbo].[comment]'))
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [fk_comment_postid]
GO
/****** Object:  ForeignKey [fk_post_catid]    Script Date: 07/27/2013 10:19:32 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_post_catid]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
ALTER TABLE [dbo].[post]  WITH CHECK ADD  CONSTRAINT [fk_post_catid] FOREIGN KEY([catid])
REFERENCES [dbo].[category] ([id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_post_catid]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
ALTER TABLE [dbo].[post] CHECK CONSTRAINT [fk_post_catid]
GO
/****** Object:  ForeignKey [fk_post_uid]    Script Date: 07/27/2013 10:19:32 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_post_uid]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
ALTER TABLE [dbo].[post]  WITH CHECK ADD  CONSTRAINT [fk_post_uid] FOREIGN KEY([uid])
REFERENCES [dbo].[member] ([id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_post_uid]') AND parent_object_id = OBJECT_ID(N'[dbo].[post]'))
ALTER TABLE [dbo].[post] CHECK CONSTRAINT [fk_post_uid]
GO
