﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormBlog
{
    public partial class Category : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int catid = Convert.ToInt32(Request.QueryString["id"]);
            if (catid <= 0)
            {
                throw new HttpException(404, "Category not found");
            }
            /*
            BlogDataSetTableAdapters.categoryTableAdapter cta = new BlogDataSetTableAdapters.categoryTableAdapter();
            BlogDataSet.categoryDataTable cdt = cta.GetDataById(catid);
            if (cdt.Rows.Count < 1)
            {
                throw new HttpException(404, "Category not found");
            }
            PageTitleLiteral.Text = cdt.Rows[0]["title"].ToString();
            CategoryNameLiteral.Text = cdt.Rows[0]["title"].ToString();

            BlogDataSetTableAdapters.postTableAdapter pta = new BlogDataSetTableAdapters.postTableAdapter();
            PostListRepeater.DataSource = pta.GetDataByCategoryId(catid);
            PostListRepeater.DataBind();
            */

            BlogModel.BlogEntities db = new BlogModel.BlogEntities();
            var category = db.categories.SingleOrDefault(t => t.id == catid);
            if (category == null)
                throw new HttpException(404, "Category not found");
            PageTitleLiteral.Text = category.title;
            CategoryNameLiteral.Text = category.title;

            var posts = from post in db.posts
                        where post.catid == catid
                        orderby post.id descending
                        select post;
            PostListRepeater.DataSource = posts;
            PostListRepeater.DataBind();

        }
    }
}