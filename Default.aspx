﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="WebFormBlog.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" Runat="Server">博客首页</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="main" Runat="Server">
<ul id="article_list">
<asp:ListView ID="PostListView" runat="server">
    <ItemTemplate>
     <li class="list_item" id="post-<%#Eval("id") %>">
         <div class="article">
             <div class="inner">
                 <h2 class="article_title title"><a href="Post.aspx?id=<%#Eval("id") %>" title="<%#Eval("title") %>"><%#Eval("title") %></a></h2>
                 <div class="article_meta">
                    <span>on</span>
                    <%# Convert.ToDateTime(Eval("created_at")).ToString("yyyy-MM-dd") %>
                 </div>
                 <div class="text">
                     <%# (Eval("excerpt") == null || Eval("excerpt") == DBNull.Value) ? Eval("content") : Eval("excerpt") %>
                 </div>
                 <div class="article_meta">
                     <span>Category: </span>
                     <a class="tag" href="Category.aspx?id=<%# Eval("catid") %>"><%# Eval("cattitle") %></a>
                 </div>
             </div>
         </div>
         <div class="sep"></div>
     </li>
    </ItemTemplate>
</asp:ListView>
</ul>
<asp:DataPager ID="PostListPager" ClientIDMode="Static" PagedControlID="PostListView" PageSize="3" QueryStringField="page" runat="server">
    <Fields>
        <asp:TemplatePagerField>
            <PagerTemplate>
                <a href="?page=<%# Container.StartRowIndex / Container.PageSize %>" id="prev" class="v_nav" style="display:<%# Container.StartRowIndex / Container.PageSize >= 1 ? "block" : "none"  %>"><span class="icon">Prev</span></a>
                <a href="?page=<%# Container.StartRowIndex / Container.PageSize + 2 %>" id="next" class="v_nav" style="display:<%# (Container.StartRowIndex / Container.PageSize + 1) < Math.Ceiling((double)Container.TotalRowCount / Container.PageSize) ? "block" : "none" %>"><span class="icon">Prev</span></a>
            </PagerTemplate>
        </asp:TemplatePagerField>
    </Fields>
</asp:DataPager>
</asp:Content>