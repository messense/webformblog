﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="Post.aspx.cs" Inherits="WebFormBlog.Post" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" Runat="Server"><asp:Literal ID="PageTitleLiteral" runat="server"></asp:Literal></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="main" Runat="Server">
<div class="article">
    <div class="inner">
        <asp:Repeater ID="PostRepeater" runat="server">
            <ItemTemplate>
            <h2 class="article_title title"><a href="#"><%# Eval("title") %></a></h2>
            <div class="article_meta">
                <div class="share">
                     <a href="https://twitter.com/share" class="twitter-share-button" data-lang="zh-cn">发推</a>
                </div>
                <span>Published on</span>
                <%# Convert.ToDateTime(Eval("created_at")).ToString("yyyy-MM-dd") %>
            </div>
            <div class="text">
                <%# Eval("content") %>
            </div>
            <div class="big_sep"></div>
            <div class="article_meta">
                <span>Category: </span>
                 <a class="tag" href="Category.aspx?id=<%# Eval("catid") %>"><%# Eval("cattitle") %></a>
            </div>
            <div class="big_sep"></div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<a id="prev" class="v_nav" clientidmode="Static" runat="server">
    <span class="icon">Prev</span>
</a>
<a id="next" class="v_nav" clientidmode="Static" runat="server">
    <span class="icon">Next</span>
</a>
</asp:Content>

