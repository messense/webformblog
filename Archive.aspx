﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="Archive.aspx.cs" Inherits="WebFormBlog.Archive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" Runat="Server"><asp:Literal ID="PageTitleLiteral" runat="server"></asp:Literal></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="main" Runat="Server">
<div class="article">
    <div class="inner">
        <h2 class="title">Posts of the year <asp:Literal ID="YearLiteral" runat="server"></asp:Literal></h2>
        <div class="text">
            <ul>
            <asp:Repeater ID="PostListRepeater" runat="server">
                <ItemTemplate>
                <li>
                    <span class="time_stamp"><%# Convert.ToDateTime(Eval("created_at")).ToString("yyyy-MM-dd") %></span>
                    <a href="post.aspx?id=<%# Eval("id") %>" title="<%# Eval("title") %>"><%# Eval("title") %></a>
                </li>
                </ItemTemplate>
            </asp:Repeater>
            </ul>
        </div>
    </div>
</div>
<a id="prev" class="v_nav" clientidmode="Static" runat="server">
    <span class="icon">Prev</span>
</a>
<a id="next" class="v_nav" clientidmode="Static" runat="server">
    <span class="icon">Next</span>
</a>
</asp:Content>

